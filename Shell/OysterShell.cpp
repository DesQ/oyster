/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "AudioManager.hpp"
#include "DisplayManager.hpp"
#include "PowerManager.hpp"

#include "OysterShell.hpp"

#include "ClockWidget.hpp"

#include "X11Handler.hpp"
#include "HotKeyManager.hpp"

#include <X11/XF86keysym.h>
#include <X11/keysymdef.h>

#include <libdesq/DesQWMSession.hpp>
#include <libdesq/DesQHotkey.hpp>
#include <libdesq/DesQSettings.hpp>

OysterShell::OysterShell( QString imageLoc ) {

	/* Store the path to the shell background */
	shellImageLoc = imageLoc;

	/* No corner selected */
	mHotCorner = -1;

	/* We need our desktop to fill the whole physical screen */
	setFixedSize( qApp->primaryScreen()->size() );

	/* If the display manager is X11, we need to let it know that we are a desktop */
	setAttribute( Qt::WA_X11NetWmWindowTypeDesktop );

	/* Title */
	setWindowTitle( "Oyster Shell" );

	/* No frame, and stay at bottom */
    setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint );

	/* Background image */
    if ( shellImageLoc.isEmpty() )
		shellImageLoc = ( QString )DesQSettings::instance( "Oyster", "Shell" )->value( "Background" );

	shellImage = QPixmap( shellImageLoc ).scaled( qApp->primaryScreen()->size(),  Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation );

	/* Create the Shell UI */
	createShellInterface();

	/* Create the desktop widgets */
	createDesktopWidgets();

	/* Load the X11 */
	loadX11();

	/* Load the HotKey Manager and the HotKeys */
	loadHotKeys();
};

void OysterShell::createShellInterface() {

	/* A Start Button at bottom right corner */
	startBtn = new QToolButton( this );
	startBtn->setIcon( QIcon( ":/icons/oyster.png" ) );
	startBtn->setIconSize( QSize( 64, 64 ) );
	startBtn->setAutoRaise( true );
	startBtn->setFocusPolicy( Qt::NoFocus );
	startBtn->setEnabled( true );
	startBtn->setAttribute( Qt::WA_TranslucentBackground, true );

	shellMenu = new OysterMenu( this );
	DesQHotkey *menuHotkey = new DesQHotkey( this );
	menuHotkey->setShortcut( tr( "Alt+F1" ) );
	connect( menuHotkey, &DesQHotkey::activated, shellMenu, &OysterMenu::show );
	connect( menuHotkey, &DesQHotkey::activated, startBtn, &QToolButton::click );

	/* Show the menu when clicked */
	connect( startBtn, &QPushButton::clicked, this, &OysterShell::showMenu );
	connect( startBtn, &QPushButton::clicked, [=]() {
		/* In case it's floating */
		if ( not startBtn->parent() ) {
			startBtn->setParent( this );
			startBtn->setWindowFlags( Qt::Widget );
			startBtn->show();
		}
	} );

	QVBoxLayout *lyt = new QVBoxLayout();
	lyt->addStretch();
	lyt->addWidget( startBtn );

	desktopLyt = new QGridLayout();
	desktopLyt->addLayout( lyt, 0, 0, Qt::AlignLeft );

	setLayout( desktopLyt );
};

void OysterShell::createDesktopWidgets() {

	QVBoxLayout *widgetsLyt = new QVBoxLayout();

	ClockWidget *cw = new ClockWidget( this );
	cw->setFixedSize( 200, 200 );

	widgetsLyt->addWidget( cw );
	widgetsLyt->addStretch();

	desktopLyt->addLayout( widgetsLyt, 0, 0, Qt::AlignCenter );

	/* Audio Popup Widget */
	audioMgr = AudioManager::instance();
	displayMgr = DisplayManager::instance();
	powerMgr = PowerManager::instance();
};

void OysterShell::loadX11() {

	X11 = X11Handler::session();

	connect( X11, &X11Handler::resizeDesktop, this, &OysterShell::resizeDesktop );
	connect( X11, &X11Handler::hotCornerActivated, this, &OysterShell::handleHotCorner );
	connect( X11, &X11Handler::hotCornerDeactivated, this, &OysterShell::cancelHotCorner );
};

void OysterShell::loadHotKeys() {

	hkMgr = HotKeyManager::instance();
	connect( hkMgr, &HotKeyManager::hotKeyPressed, this, &OysterShell::handleHotKey );

	/* Brightness up/down */
	hkMgr->registerHotKey( 0, XF86XK_MonBrightnessUp );
	hkMgr->registerHotKey( ControlMask | ShiftMask, XK_D );
	hkMgr->registerHotKey( 0, XF86XK_MonBrightnessDown );
	hkMgr->registerHotKey( ControlMask | ShiftMask, XK_F );

	/* Volume up/down/mute */
	hkMgr->registerHotKey( 0, XF86XK_AudioMute );
	hkMgr->registerHotKey( 0, XF86XK_AudioLowerVolume );
	hkMgr->registerHotKey( ControlMask | ShiftMask, XK_S );
	hkMgr->registerHotKey( 0, XF86XK_AudioRaiseVolume );
	hkMgr->registerHotKey( ControlMask | ShiftMask, XK_A );

	hkMgr->start();
};

void OysterShell::handleHotKey( quint32 id ) {

	switch( id ) {
		case XF86XK_MonBrightnessUp: {
			displayMgr->increaseBrightness();
			break;
		}

		case XF86XK_MonBrightnessDown: {
			displayMgr->decreaseBrightness();
			break;
		}

		case XF86XK_AudioMute: {
			audioMgr->toggleMute();
			break;
		}

		case XF86XK_AudioLowerVolume: {
			audioMgr->decreaseVolume();
			break;
		}

		case XF86XK_AudioRaiseVolume: {
			audioMgr->increaseVolume();
			break;
		}

		default: {
			break;
		}
	}
};

void OysterShell::handleHotCorner( int corner ) {

	qDebug() << "In hot corner" << corner;

	mHotCorner = corner;

	/* Relax 500ms before activating the hot corner */
	hotCornerTimer.start( 250, this );
};

void OysterShell::cancelHotCorner( int corner ) {

	mHotCorner = -1;

	switch( corner ) {
		case Qt::TopLeftCorner: {
			qDebug() << "Leaving Top Left Corner";
			break;
		}

		/* We are exiting from the TopRight corner, so show restore the window state */
		case Qt::TopRightCorner: {
			qDebug() << "Leaving Top Right Corner";
			break;
		}

		case Qt::BottomLeftCorner: {
			qDebug() << "Leaving Bottom Left Corner";
			break;
		}

		case Qt::BottomRightCorner: {
			qDebug() << "Leaving Bottom Right Corner";
			break;
		}

		/* In case hot corner was canceled */
		default: {
			break;
		}
	}

	/* Stop the hot corner activation */
	hotCornerTimer.stop();
};

void OysterShell::showMenu() {

	shellMenu->setFixedSize( qApp->primaryScreen()->size() );
	shellMenu->show();
};

void OysterShell::lockScreen() {

	qDebug() << "Locking screen";
	// ScreenLock *lock = new ScreenLock();
	// lock->exec();
};

void OysterShell::resizeDesktop() {

	/* We need our desktop to fill the whole physical screen */
	setFixedSize( qApp->primaryScreen()->size() );

	/* Rescale the background image */
	shellImage = QPixmap( shellImageLoc ).scaled( qApp->primaryScreen()->size(),  Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation );

	/* Force repaint; because bg has changed */
	repaint();
};

void OysterShell::paintEvent( QPaintEvent *pEvent ) {

	QPainter painter( this );
	painter.drawPixmap( geometry(), shellImage, QRect( QPoint( ( shellImage.width() - width() ) / 2, ( shellImage.height() - height() ) / 2 ), size() ) );
	painter.end();

	QWidget::paintEvent( pEvent );

	pEvent->accept();
};

void OysterShell::timerEvent( QTimerEvent *tEvent ) {

	if ( tEvent->timerId() == hotCornerTimer.timerId() ) {
		/* First stop the timer */
		hotCornerTimer.stop();

		/* Based on the corner take action */
		switch( mHotCorner ) {
			case Qt::TopLeftCorner: {
				qDebug() << "Entering Top Left Corner";
				X11->showDesktop();
				break;
			}

			case Qt::TopRightCorner: {
				qDebug() << "Entering Top Right Corner";
				/* Use TasksPlugin to show the list widget */
				X11->showClientList();
				break;
			}

			case Qt::BottomLeftCorner: {
				qDebug() << "Entering Bottom Left Corner";
				if ( startBtn->parent() ) {
					startBtn->setParent( nullptr );
					startBtn->setWindowFlags( Qt::Widget | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint );
					startBtn->setAttribute( Qt::WA_X11NetWmWindowTypeDock );
					startBtn->show();
				}

				else {
					startBtn->setParent( this );
					startBtn->setWindowFlags( Qt::Widget );
					startBtn->setAttribute( Qt::WA_X11NetWmWindowTypeDock, false );
					startBtn->show();
				}

				break;
			}

			case Qt::BottomRightCorner: {
				qDebug() << "Entering Bottom Right Corner";
				qDebug() << "Locking screen";
				// ScreenLock *lock = new ScreenLock();
				// lock->exec();
				break;
			}

			/* In case hot corner was canceled */
			default: {
				tEvent->ignore();
				break;
			}
		}

		tEvent->accept();
		return;
	}

	QWidget::timerEvent( tEvent );
};
