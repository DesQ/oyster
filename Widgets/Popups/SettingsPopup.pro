TEMPLATE = app
TARGET = SettingsPopup

INCLUDEPATH += .
CONFIG += silent

QT += quick

# Input
SOURCES += main.cpp

RESOURCES += SettingsPopup.qrc
