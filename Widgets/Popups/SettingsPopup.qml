import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Qt.labs.platform 1.1

Window {
    id: settingsPopup
    visible: false
    width: 256
    height: 256
    flags: Qt.FramelessWindowHint | Qt.Window | Qt.WindowStaysOnTopHint
    color: "transparent"

	Timer {
		id: closeTimer;
		objectName: "closeTimer";

		interval: 1000;

		onTriggered: {
			close();
		}
	}

	MouseArea {
	    anchors.fill: parent
		propagateComposedEvents: true
	    hoverEnabled: false

	    onClicked:{
			closeTimer.stop()
		}
	}

    Rectangle {
        color: "#30ffffff"
        anchors.fill: parent
        anchors.margins: 10
        radius: 10

		ColumnLayout {
			id: volumeLyt
	        anchors.fill: parent

			Image {
	            id: settingsIcon
				objectName: "settingsIcon"
				Layout.alignment: Qt.AlignCenter

	            width: 128
	            height: 128

	            source: Qt.resolvedUrl( "qrc:///icons/oyster.png" )
	            sourceSize.width: 128
	            sourceSize.height: 128
	            fillMode: Image.PreserveAspectFit
	        }

			SettingsSlider {
				id: slider
				objectName: "slider"
				Layout.alignment: Qt.AlignHCenter
				height: 27
				value: 0.2

				onClicked: {
					closeTimer.stop()
				}

				onCompleted: {
					console.log( "restarting the timer" );
					closeTimer.start();
				}
			}
		}
    }
}
