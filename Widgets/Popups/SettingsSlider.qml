import QtQuick 2.0

Item {
    id: root

    property double maximum:  1.0
    property double value:    0.5
    property double minimum:  0.0

    signal clicked( double value );
    signal completed();

    width: 128
	height: 36

	Rectangle {
		id: base
		anchors.fill: parent

		width: root.width
		height: root.height
		color: "white"
		radius: 0.5 * height

		Rectangle {
			id: pill
			x: parent.x + 1
			y: parent.y + 1
			height: parent.height - 2
			color: "gray"
			radius: 0.5 * height

			width: value * ( parent.width - 2 )
		}
	}

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        drag {
            target:   pill
            axis:     Drag.XAxis
            maximumX: base.width - 2
            minimumX: 0
        }

        onPositionChanged: {
			if ( drag.active )
				setPixels( pill.x + 0.5 * pill.width )
		}

        onClicked: {
			setPixels( mouse.x )
		}

		onReleased: {
			completed();
		}
    }

    function setPixels( pixels ) {

		if ( pixels < 1 )
			pill.width = 0

		else if ( pixels > base.width - 2 )
			pill.width = base.width - 2

		else
			pill.width = pixels

		pill.x = base.x + 1
		root.value = pill.width / ( base.width - 2 )

        clicked( root.value )
    }
}
