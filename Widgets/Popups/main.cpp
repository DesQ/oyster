#include <QtGui>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QDebug>
#include <unistd.h>

int main( int argc, char *argv[] ) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url( "qrc:/SettingsPopup.qml" );

    QObject::connect(
		&engine, &QQmlApplicationEngine::objectCreated, &app, [url] ( QObject *obj, const QUrl &objUrl ) {
			if ( !obj && url == objUrl )
				QCoreApplication::exit( -1 ) ;
		}, Qt::QueuedConnection
	);

    engine.load( url );
	qApp->processEvents();

	QQmlComponent component( &engine, url );
	QObject *object = component.create();

	QMetaObject::invokeMethod( object, "show" );
	qApp->processEvents();

    return app.exec();
}
