/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of Oyster project ( https://gitlab.com/Oyster/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "OysterMenuViews.hpp"

OysterCategoryView::OysterCategoryView( QWidget *parent ) : QScrollArea( parent ) {

	setFrameStyle( QFrame::NoFrame );
	setWidgetResizable( true );
	setFixedWidth( 48 );
	setAlignment( Qt::AlignHCenter );
	viewport()->setAutoFillBackground( false );
	setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
	setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

	QStringList categories = {
		"All Applications", "Accessories", "Development", "Education", "Games", "Graphics",
		"Internet", "Multimedia", "Office", "Science & Math", "Settings", "System", "Wine"
	};

	QStringList categoryIcons = {
		"preferences-system-windows", "applications-utilities", "applications-development", "applications-education", "applications-games", "applications-graphics",
		"applications-internet", "applications-multimedia", "applications-office", "applications-science", "preferences-other", "applications-system",
		"wine"
	};

	QVBoxLayout *ctgLyt = new QVBoxLayout();
	ctgLyt->setContentsMargins( QMargins() );
	ctgLyt->setSpacing( 0 );

	ctgGrp = new QButtonGroup();
	ctgGrp->setExclusive( true );

	for( int i = 0; i < categories.count(); i++ ) {
		QToolButton *ctgBtn = new QToolButton();
		ctgBtn->setIcon( QIcon::fromTheme( categoryIcons.value( i ) ) );
		ctgBtn->setText( categories.value( i ) );
		ctgBtn->setToolTip( categories.value( i ) );

		/* Button properties */
		ctgBtn->setFocusPolicy( Qt::NoFocus );
		ctgBtn->setAutoRaise( true );
		ctgBtn->setToolButtonStyle( Qt::ToolButtonIconOnly );
		ctgBtn->setIconSize( QSize( 32, 32 ) );
		ctgBtn->setFixedSize( QSize( 36, 36 ) );
		ctgBtn->setCheckable( true );

		connect( ctgBtn, &QToolButton::clicked, this, [=]() {
				emit loadCategory( categories.value( i ) );
			}
		);

		ctgLyt->addWidget( ctgBtn );
		ctgGrp->addButton( ctgBtn, i );
	}
	ctgLyt->addStretch();

	QWidget *base = new QWidget();
	base->setLayout( ctgLyt );

	setWidget( base );

	ctgGrp->button( 0 )->click();
};

void OysterCategoryView::setCategories( QStringList categories ) {

	qDebug() << categories;

	if ( not categories.contains( "All Applications" ) )
		categories << "All Applications";

	for( int r = 0; r < 13; r++ ) {
		QToolButton *btn = qobject_cast<QToolButton *>( ctgGrp->button( r ) );
		qDebug() << btn->text();
		if ( not categories.contains( btn->text() ) )
			btn->setDisabled( true );

		else
			btn->setEnabled( true );
	}
};

OysterApplicationsView::OysterApplicationsView( QWidget *parent ) : QListWidget( parent ) {

	setViewMode( QListView::IconMode );
	setFlow( QListView::LeftToRight );
	setWrapping( true );
	setWordWrap( false );
	setTextElideMode( Qt::ElideRight );
	setResizeMode( QListView::Adjust );
	setMovement( QListView::Static );
	setIconSize( QSize( 64, 64 ) );
	setGridSize( QSize( 144, 108 ) );
	setFrameStyle( QFrame::NoFrame );
	setUniformItemSizes( true );
	viewport()->setAutoFillBackground( false );

	setItemDelegate( new OysterAppsViewDelegate() );

	parseDesktops();
	loadCategory( "All Applications" );

	QFileSystemWatcher *fsw = new QFileSystemWatcher();
	fsw->addPaths( {
		QDir::home().filePath( ".local/share/applications/" ),
		"/usr/local/share/applications/",
		"/usr/share/applications/" ,
		QDir::home().filePath( ".local/share/games/" ),
		"/usr/local/share/games/",
		"/usr/share/games/"
	} );

	connect( fsw, &QFileSystemWatcher::directoryChanged, [=]( QString ) {
		parseDesktops();
		loadCategory( mCurCategory );
	} );

	connect( this, &QListWidget::itemClicked, this, [=]( QListWidgetItem *item ) {

			QString desktopUrl = item->data( Qt::UserRole + 1 ).toString();

			DesQDesktopFile appFile( desktopUrl );
			appFile.startApplicationWithArgs( {} );

			emit closeMenu();
		}
	);
};

void OysterApplicationsView::loadCategory( QString category ) {

	clear();

	mCurCategory = category;
	if ( category == "All Applications" ) {
		Q_FOREACH( DesQDesktopFile app, allApps ) {

			if ( not mFilter.isEmpty() ) {
				bool matches = false;
				matches |= app.name().contains( mFilter, Qt::CaseInsensitive );
				matches |= app.desktopName().contains( mFilter, Qt::CaseInsensitive );
				matches |= app.command().contains( mFilter, Qt::CaseInsensitive );

				if ( not matches )
					continue;
			}

			QString name = app.name();
			QString icoStr = app.icon();

			/*
				*
				* QIcon bug:
				* If @icoStr starts with /, QIcon::isNull() always returns false,
				* even when we use QIcon::fromTheme( ... )
				*
				* So first check if it begins with /
				*
			*/
			QIcon icon;
			if ( icoStr.startsWith( "/" ) ) {
				if ( DesQUtils::exists( icoStr ) ) {

					icon = QIcon( icoStr );
				}

				else {

					icon = QIcon::fromTheme( "application-x-executable" );
				}
			}

			else if ( QIcon::hasThemeIcon( icoStr ) ) {

				icon = QIcon::fromTheme( icoStr );
			}

			else {

				icon = QIcon::fromTheme( "application-x-executable" );
			}

			QListWidgetItem *item = new QListWidgetItem( icon, name, this );
			item->setData( Qt::UserRole + 1, app.desktopFileUrl() );
			// item->setTextAlignment( Qt::AlignVCenter | Qt::AlignLeft );

			addItem( item );
		}
	}

	else {
		Q_FOREACH( DesQDesktopFile app, categoryAppsMap[ category ] ) {

			if ( not mFilter.isEmpty() ) {
				bool matches = false;
				matches |= app.name().contains( mFilter, Qt::CaseInsensitive );
				matches |= app.desktopName().contains( mFilter, Qt::CaseInsensitive );
				matches |= app.command().contains( mFilter, Qt::CaseInsensitive );

				if ( not matches )
					continue;
			}

			QString name = app.name();
			QString icoStr = app.icon();

			/* Read diatribe starting from line 92 above */
			QIcon icon;
			if ( icoStr.startsWith( "/" ) ) {
				if ( DesQUtils::exists( icoStr ) ) {

					icon = QIcon( icoStr );
				}

				else {

					icon = QIcon::fromTheme( "application-x-executable" );
				}
			}

			else if ( QIcon::hasThemeIcon( icoStr ) ) {

				icon = QIcon::fromTheme( icoStr );
			}

			else {

				icon = QIcon::fromTheme( "application-x-executable" );
			}

			QListWidgetItem *item = new QListWidgetItem( icon, name, this );
			item->setData( Qt::UserRole + 1, app.desktopFileUrl() );
			// item->setTextAlignment( Qt::AlignVCenter | Qt::AlignLeft );

			addItem( item );
		}
	}
};

void OysterApplicationsView::setFilter( QString filter ) {

	mFilter = filter;
	loadCategory( mCurCategory );
};

QStringList OysterApplicationsView::availableCategories() {

	return mAvailableCategories;
};

void OysterApplicationsView::parseDesktops() {

	categoryAppsMap.clear();
	allApps.clear();
	mAvailableCategories.clear();

	/* Apps */
	QStringList appsDirs = {
		QDir::home().filePath( ".local/share/applications/" ),
		"/usr/local/share/applications/",
		"/usr/share/applications/"
	};

	Q_FOREACH( QString appDirLoc, appsDirs ) {
		QDir appDir( appDirLoc );
		appDir.setFilter( QDir::Files );
		appDir.setNameFilters( { "*.desktop" } );

		Q_FOREACH( QString app, appDir.entryList() ) {
			DesQDesktopFile desktopFile( appDir.filePath( app ) );

			QString category = desktopFile.category();
			QString desktopUrl = appDir.filePath( app );

			if ( not desktopFile.isValid() )
				continue;

			if ( desktopFile.type() != DesQDesktopFile::Type::Application )
				continue;

			if ( desktopFile.visible() ) {
				allApps << desktopFile;

				if ( not categoryAppsMap.contains( category ) )
					categoryAppsMap[ category ] = DesQAppsList();

				categoryAppsMap[ category ] << desktopFile;
			}
		}
	}

	/* Games */
	QStringList gamesDirs = {
		QDir::home().filePath( ".local/share/games/" ),
		"/usr/local/share/games/",
		"/usr/share/games/"
	};

	Q_FOREACH( QString gameDirLoc, gamesDirs ) {
		QDir gameDir( gameDirLoc );
		gameDir.setFilter( QDir::Files );
		gameDir.setNameFilters( { "*.desktop" } );

		Q_FOREACH( QString app, gameDir.entryList() ) {
			DesQDesktopFile desktopFile( gameDir.filePath( app ) );

			QString category = desktopFile.category();
			QString desktopUrl = gameDir.filePath( app );

			if ( not desktopFile.isValid() )
				continue;

			if ( desktopFile.type() != DesQDesktopFile::Type::Application )
				continue;

			if ( desktopFile.visible() ) {
				allApps << desktopFile;

				if ( not categoryAppsMap.contains( "Games" ) )
					categoryAppsMap[ "Games" ] = DesQAppsList();

				categoryAppsMap[ "Games" ] << desktopFile;
			}
		}
	}

	/* Wine */
	QDirIterator wineDirIt( QDir::home().filePath( ".local/share/applications/wine/" ), { "*.desktop" }, QDir::Files, QDirIterator::Subdirectories );
	while( wineDirIt.hasNext() ) {

		DesQDesktopFile desktopFile( wineDirIt.next() );

		QString category = desktopFile.category();
		QString desktopUrl = wineDirIt.filePath();

		if ( not desktopFile.isValid() )
			continue;

		if ( desktopFile.type() != DesQDesktopFile::Type::Application )
			continue;

		if ( desktopFile.visible() ) {
			allApps << desktopFile;

			if ( not categoryAppsMap.contains( "Wine" ) )
				categoryAppsMap[ "Wine" ] = DesQAppsList();

			categoryAppsMap[ "Wine" ] << desktopFile;
		}
	}

	mAvailableCategories << categoryAppsMap.keys();

	qSort( allApps.begin(), allApps.end(), [=]( const DesQDesktopFile first, const DesQDesktopFile second ) {

		return first.name().toLower() < second.name().toLower() ;
	} );

	Q_FOREACH( QString category, mAvailableCategories ) {
		qSort( categoryAppsMap[ category ].begin(), categoryAppsMap[ category ].end(), [=]( const DesQDesktopFile first, const DesQDesktopFile second ) {

				return first.name().toLower() < second.name().toLower() ;
			} );
	}
};

void OysterApplicationsView::mouseReleaseEvent( QMouseEvent *mEvent ) {

	if ( not itemAt( mEvent->pos() ) )
		emit closeMenu();

	QListWidget::mouseReleaseEvent( mEvent );
};

void OysterApplicationsView::paintEvent( QPaintEvent *pEvent ) {

	QPainter painter( viewport() );
	painter.setPen( Qt::gray );
	painter.drawLine( geometry().topRight(), geometry().bottomRight() );
	painter.end();

	QListWidget::paintEvent( pEvent );
};

void OysterApplicationsView::resizeEvent( QResizeEvent *rEvent ) {

	rEvent->accept();

	QSize vSize = viewport()->size();

	/* Items per row */
	float items = vSize.width() / ( 144 + spacing() );

	/* Minimum width of all items */
	float itemsWidth  = items * 144;

	/* Empty space remaining */
	float empty = vSize.width() - itemsWidth;

	/* Extra space per item */
	float extra = ( empty / items ) - 5;

	/* New grid size */
	setGridSize( QSize( 144 + extra, 108 ) );
};

QSize OysterAppsViewDelegate::sizeHint( const QStyleOptionViewItem &option, const QModelIndex &index ) const {

	QSize size( QStyledItemDelegate::sizeHint( option, index ) );

	/* Icon View */
	if ( option.decorationPosition == QStyleOptionViewItem::Top ) {
        // size.setWidth( static_cast<int>(FileSystemView::gridWidth) - 5 );
		return QSize( 128, 96 );
	}

	/* Tile View */
	else if ( option.decorationPosition == QStyleOptionViewItem::Left ) {
        // size.setWidth( static_cast<int>(FileSystemView::gridWidth) - 5 );
		return QSize( 256, 72 );
	}

	return size;
};
