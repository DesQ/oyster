/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of Oyster project ( https://gitlab.com/Oyster/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "OysterMenu.hpp"

#include <libdesq/DesQWMSession.hpp>

OysterMenu::OysterMenu( QWidget *parent ) : QWidget( parent ) {

	/* We need our desktop to fill the whole physical screen */
	setFixedSize( qApp->primaryScreen()->size() );

	/* Title */
	setWindowTitle( "Oyster Menu" );

	/* No frame, and stay on top */
    setWindowFlags( Qt::Popup | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint );

    // if ( OysterWMSession::currentSession()->isCompositorRunning() )
	setAttribute( Qt::WA_TranslucentBackground );

	/* Populate the menu */
	createUI();
};

void OysterMenu::createUI() {

	searchEdit = new QLineEdit();
	searchEdit->setPlaceholderText( "Type to search..." );
	searchEdit->setFont( QFont( font().family(), 20 ) );
	searchEdit->setAlignment( Qt::AlignCenter );
	searchEdit->setFrame( QFrame::NoFrame );
	searchEdit->setStyleSheet( "background: transparent; margin-bottom: 10px;" );
	searchEdit->grabKeyboard();
	searchEdit->setMinimumWidth( 300 );

	catsView = new OysterCategoryView( this );
	appsView = new OysterApplicationsView( this );

	connect(
		searchEdit, &QLineEdit::textChanged, this, [=]( QString filter ) {
			appsView->setFilter( filter );

			if ( filter.isEmpty() )
				catsView->setEnabled( true );

			else
				catsView->setDisabled( true );

			catsView->loadCategory( "All Applications" );
		}
	);

	connect( appsView, &OysterApplicationsView::menuReloaded, [=]() {
		catsView->setCategories( appsView->availableCategories() );
	} );

	connect( catsView, &OysterCategoryView::loadCategory, appsView, &OysterApplicationsView::loadCategory );
	connect( catsView, &OysterCategoryView::closeMenu, this, &OysterMenu::close );
	connect( appsView, &OysterApplicationsView::closeMenu, this, &OysterMenu::close );

	QGridLayout *viewLyt = new QGridLayout();
	viewLyt->setContentsMargins( 5, 15, 5, 15 );
	viewLyt->setSpacing( 0 );
	viewLyt->addWidget( catsView, 1, 0 );
	viewLyt->addWidget( appsView, 1, 1 );

	QFrame *menuFrame = new QFrame();
	menuFrame->setLayout( viewLyt );
	menuFrame->setObjectName( "frame" );
	menuFrame->setStyleSheet( "#frame{ border: none; border-top: 1px solid gray;; border-bottom: 1px solid gray; }" );

	QToolButton *powerBtn = new QToolButton();
	powerBtn->setIcon( QIcon::fromTheme( "system-shutdown" ) );
	powerBtn->setIconSize( QSize( 36, 36 ) );
	powerBtn->setAutoRaise( true );

	/* Power Menu */
	QMenu *powerMenu = new QMenu( powerBtn );
	powerMenu->addAction(
		QIcon::fromTheme( "system-lock-screen" ), "Lock Screen", [=]() {
			QProcess::startDetached( "loginctl", { "lock-session" } );
			close();
		}
	);

	powerMenu->addAction(
		QIcon::fromTheme( "system-log-out" ), "Log Out", [=]() {
			QProcess proc;
			proc.start( "desq-session-x11", QStringList() << "--logout" );
			proc.waitForFinished();

			/* We manually close this app: May not be necessary */
			qApp->quit();
		}
	);

	powerMenu->addSeparator();

	powerMenu->addAction(
		QIcon::fromTheme( "system-suspend" ), "Suspend to RAM", [=]() {
			QProcess proc;
			proc.start( "desq-session-x11", QStringList() << "--suspend" );
			proc.waitForFinished();

			/* We manually close this app: May not be necessary */
			close();
		}
	);

	powerMenu->addAction(
		QIcon::fromTheme( "system-suspend-hibernate" ), "Suspend to Disk", [=]() {
			QProcess proc;
			proc.start( "desq-session-x11", QStringList() << "--hibernate" );
			proc.waitForFinished();

			/* We manually close this app: May not be necessary */
			close();
		}
	);

	powerMenu->addSeparator();

	powerMenu->addAction(
		QIcon::fromTheme( "system-shutdown" ), "Power off", [=]() {
			QProcess proc;
			proc.start( "desq-session-x11", QStringList() << "--shutdown" );
			proc.waitForFinished();

			/* We manually close this app: May not be necessary */
			qApp->quit();
		}
	);

	powerMenu->addAction(
		QIcon::fromTheme( "system-reboot" ), "Reboot", [=](){
			QProcess proc;
			proc.start( "desq-session-x11", QStringList() << "--reboot" );
			proc.waitForFinished();

			/* We manually close this app: May not be necessary */
			qApp->quit();
		}
	);

	powerBtn->setMenu( powerMenu );
	powerBtn->setPopupMode( QToolButton::InstantPopup );

	QGridLayout *menuLyt = new QGridLayout();
	menuLyt->addWidget( searchEdit, 0, 0, 1, 2, Qt::AlignCenter );
	menuLyt->addWidget( menuFrame, 1, 0, 1, 2 );
	menuLyt->addWidget( powerBtn, 2, 0 );

	setLayout( menuLyt );
};

void OysterMenu::mouseReleaseEvent( QMouseEvent *mEvent ) {

	QWidget::mouseReleaseEvent( mEvent );
	close();
};

void OysterMenu::paintEvent( QPaintEvent *pEvent ) {

	QPainter painter( this );

	QColor bgTint( palette().color( QPalette::Window ).darker() );
	bgTint.setAlpha( 210 );
	painter.fillRect( geometry(), bgTint );
	painter.end();

	QWidget::paintEvent( pEvent );
};

void OysterMenu::closeEvent( QCloseEvent *cEvent ) {

	emit closed();

	if ( searchEdit )
		searchEdit->releaseKeyboard();

	QWidget::closeEvent( cEvent );
};
