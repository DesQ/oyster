/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of Oyster project ( https://gitlab.com/Oyster/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include "Global.hpp"
#include <libdesq/DesQXdg.hpp>
#include <libdesq/DesQTools.hpp>

class OysterCategoryView : public QScrollArea {
	Q_OBJECT

	public:
		OysterCategoryView( QWidget *parent );
		void setCategories( QStringList );

	private:
		QButtonGroup *ctgGrp;

	Q_SIGNALS:
		void closeMenu();
		void loadCategory( QString );
};

class OysterApplicationsView : public QListWidget {
	Q_OBJECT

	public:
		OysterApplicationsView( QWidget *parent );

		void loadCategory( QString category );
		void setFilter( QString filter );

		QStringList availableCategories();

	private:
		void parseDesktops();

		DesQAppsList allApps;
		QMap<QString, DesQAppsList> categoryAppsMap;
		QString mFilter, mCurCategory;
		QStringList mAvailableCategories;

	protected:
		void mouseReleaseEvent( QMouseEvent *mEvent );
		void paintEvent( QPaintEvent *pEvent );
		void resizeEvent( QResizeEvent *rEvent );

	Q_SIGNALS:
		void closeMenu();
		void menuReloaded();
};

class OysterAppsViewDelegate : public QStyledItemDelegate {
    Q_OBJECT

	public:
		QSize sizeHint( const QStyleOptionViewItem &option, const QModelIndex &index ) const override;
};
