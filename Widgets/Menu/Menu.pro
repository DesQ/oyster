TEMPLATE = app
TARGET = MenuTrial

QT += widgets dbus
INCLUDEPATH += . ..

LIBS += -ldesq

# Input
HEADERS += DesQMenu.hpp DesQMenuViews.hpp
SOURCES += DesQMenu.cpp DesQMenuViews.cpp MenuMain.cpp

CONFIG += silent

DEFINES += TEST

# Build location
BUILD_PREFIX = $$(DESQ_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
	BUILD_PREFIX = ./build
}

MOC_DIR 	= $$BUILD_PREFIX/Shell/moc
OBJECTS_DIR = $$BUILD_PREFIX/Shell/obj
RCC_DIR		= $$BUILD_PREFIX/Shell/qrc
UI_DIR      = $$BUILD_PREFIX/Shell/uic
