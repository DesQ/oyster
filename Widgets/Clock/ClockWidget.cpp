/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	* Qt5 example of analog clock, and Lenovo K8 Note's clock widget
	* are the inspiration for this. Some parts of the code are taken
	* from Qt5's example
	*
	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*
	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*
	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QtWidgets>
#include "ClockWidget.hpp"

ClockWidget::ClockWidget( QWidget *parent ) : QWidget( parent ) {

	mBatMgr = BatteryManager::instance();
	mBatMgr->refreshBatteries();

	battpc = 100;

	timer = new QBasicTimer();
	timer->start( 1000, this );

	setWindowTitle( tr( "Analog Clock" ) );
	resize( 200, 200 );
};

void ClockWidget::timerEvent( QTimerEvent *tEvent ) {

	if ( tEvent->timerId() == timer->timerId() ) {
		if ( mBatMgr->hasBatteries() ) {
			Battery mainBattery = mBatMgr->batteries().at( 0 );
			battpc = mainBattery.value( "Percentage" ).toInt();

			if ( ( battpc <= 20.0 ) and ( not warned ) ) {
				QMessageBox::information(
					this,
					"Oyster | Battery Warning",
					"Your battery is low. Please charge your device to continue working on it."
				);

				warned = true;
			}

			if ( battpc <= 5.0 ) {
				QMessageBox::information(
					this,
					"Oyster | Battery Critical",
					"Your battery is critical. Your device will shutdown in 60s."
				);

				system( "shutdown -h 60" );
			}
		}

		repaint();
	}

	QWidget::timerEvent( tEvent );
};

void ClockWidget::paintEvent( QPaintEvent * ) {

	QPainter painter( this );
	painter.setRenderHint( QPainter::Antialiasing );

	/* Size of the square */
	int side = qMin( width() - 20, height() - 20 );

	/* Draw battery circle */
	if ( mBatMgr->hasBatteries() ) {
		QColor battColor = ( battpc <= 10 ? Qt::darkRed : ( battpc <= 50 ? Qt::darkYellow : Qt::white ) );

		painter.save();
		painter.setPen( QPen( battColor, ( side > 100 ? 8 : ( side > 50 ? 4 : 2 ) ), Qt::SolidLine, Qt::RoundCap ) );
		painter.drawArc( QRect( (width() - side) / 2, (height() - side) / 2, side, side ), 90.0 * 16.0, -360.0 * 16.0 * battpc / 100.0 );
		painter.restore();
	}

	/* Draw an empty shell? */
	else {
		painter.save();
		painter.setPen( QPen( Qt::white, 2, Qt::SolidLine, Qt::RoundCap ) );
		painter.drawArc( QRect( (width() - side) / 2, (height() - side) / 2, side, side ), 90.0 * 16.0, -360.0 * 16.0 );
		painter.restore();
	}

	QTime time = QTime::currentTime();

	painter.translate( width() / 2, height() / 2 );
	painter.scale( ( side - 10 ) / 200.0, ( side - 10 ) / 200.0 );

	painter.setPen( QPen( palette().color( QPalette::WindowText ), 2.0, Qt::SolidLine, Qt::RoundCap ) );
	for ( int i = 0; i < 12; ++i ) {
		painter.drawLine( 88, 0, 96, 0 );
		painter.rotate( 30.0 );
	}

	painter.setPen( QPen( palette().color( QPalette::WindowText ), 2.0, Qt::SolidLine, Qt::RoundCap ) );
	for ( int j = 0; j < 60; ++j ) {
		if ( ( j % 5 ) != 0 )
			painter.drawLine( 92, 0, 96, 0 );
		painter.rotate( 6.0 );
	}

	/* Hour Hand */
	painter.save();
	painter.rotate( 30.0 * ( ( time.hour() + time.minute() / 60.0 ) ) );
	painter.setPen( QPen( palette().color( QPalette::WindowText ), 5.0, Qt::SolidLine, Qt::RoundCap ) );
	painter.drawLine( QLineF( QPointF( 0, 0 ), QPointF( 0, -40 ) ) );
	painter.restore();

	/* Minute Hand */
	painter.save();
	painter.rotate( 6.0 * ( time.minute() + time.second() / 60.0 ) );
	painter.setPen( QPen( palette().color( QPalette::WindowText ), 4.0, Qt::SolidLine, Qt::RoundCap ) );
	painter.drawLine( QLineF( QPointF( 0, 0 ), QPointF( 0, -70 ) ) );
	painter.restore();
}
