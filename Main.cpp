/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

// Local Headers
#include "Global.hpp"
#include "OysterShell.hpp"

#include <libdesq/DesQTools.hpp>
#include <libdesq/DesQClient.hpp>
#include <libdesq/DesQWMSession.hpp>
#include <libdesq/DesQEventFilter.hpp>

#include <libdesqui/DesQApplication.hpp>

#include "PresentWindows.hpp"

void startXephyr( QProcess *xephyr, QProcess *compiz ) {
	xephyr->start( "Xephyr", { ":1", "-screen", "720x540" } );
	xephyr->waitForStarted();

	qputenv( "DISPLAY", ":1" );

	compiz->start( "compiz" );
	compiz->waitForStarted();

	sleep( 1 );
}

int main( int argc, char **argv ) {

	qInstallMessageHandler( Logger );

	QApplication::setAttribute( Qt::AA_EnableHighDpiScaling );

	QProcess xephyr, compiz;
	if ( ( argc >= 2 ) and ( not strcmp( argv[ 1 ], "--test" ) ) ) {
		startXephyr( &xephyr, &compiz );
	}

	DesQApplication app( argc, argv, "OysterShell" );
	QCoreApplication::instance()->installNativeEventFilter( new DesQEventFilter( DesQWMSession::currentSession() ) );

    QCommandLineParser parser;
    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

	/* Optional: Path where the search begins */
    parser.addOption( { "background", "The path to the background file", "image file" } );
    parser.addOption( { "test", "Enable OysterShell test mode on Xephyr" } );

	/* Process the CLI args */
    parser.process(app);

	OysterShell *shell;

	if ( parser.isSet( "background" ) )
		shell = new OysterShell( parser.value( "background" ) );

	else
		shell = new OysterShell();

	shell->show();
	qDebug() << "Starting oyster-shell";

	QCursor::setPos( shell->geometry().center() );

	return app.exec();
};
