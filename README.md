# Oyster Shell
### A beautiful user interface for mobiles and tablets.

Built as a fork of DesQ for PineTab and other similar devices, Oyster focuses exclusively on touch based devices.

### Notes for compiling (Qt5) - linux:

* Download the sources
  * Git: `git clone https://gitlab.com/Oyster/Shell.git OysterShell-master`
* Enter `OysterShell-master`
* Open the terminal and type: `qmake -qt5 && make`
* To install, as root type: `make install`

### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* libxcb1 (libxcb1-dev)
* libxcb-ewmh2 (libxcb-ewmh-dev)
* libxcb-icccm4 (libxcb-icccm4-dev)
* [libdesq](https://gitlab.com/DesQ/libdesq)
* [libdesqui](https://gitlab.com/DesQ/libdesqui)

## My System Info
* OS:				Debian Sid
* Qt:				Qt5 5.12.5
* libxcb1:          1.13.1
* libxcb-ewmh2:     0.4.1
* libxcb-icccm4:    0.4.1
* libpam:           1.3.1

### Known Bugs
* Pinning a window on all desktops does not work

### Upcoming
* Any other feature you request for... :)
