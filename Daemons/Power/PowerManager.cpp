/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	* Qt5 example of analog clock, and Lenovo K8 Note's clock widget
	* are the inspiration for this. Some parts of the code are taken
	* from Qt5's example
	*
	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*
	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*
	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QQmlComponent>
#include "PowerManager.hpp"

PowerManager *PowerManager::m_mgr = nullptr;

PowerManager* PowerManager::instance() {

	if ( m_mgr == nullptr )
		m_mgr = new PowerManager();

	return m_mgr;
};

PowerManager::PowerManager() : QObject() {

	QList<Solid::Device> devices = Solid::Device::listFromType( Solid::DeviceInterface::Battery, QString() );

    if ( devices.isEmpty() ) {
        qDebug() << "No battries found";
		return;
    }

    for ( Solid::Device device: devices ) {
        Solid::Battery *battery = device.as<Solid::Battery>();
        if ( battery->type() != Solid::Battery::PrimaryBattery )
            continue;

        mBatteries << battery;
		mBatteryStates << battery->chargeState();
        connect( battery, &Solid::Battery::powerSupplyStateChanged, this, &PowerManager::onChargeChanged );
        connect( battery, &Solid::Battery::chargePercentChanged, this, &PowerManager::onChargeChanged );
    }

	const QUrl url( "qrc:/InfoPopup.qml" );
	view = new QQmlApplicationEngine();
	qApp->processEvents();

	QQmlComponent component( view, url );
	popup = component.create();
	icon  = popup->findChild<QObject *>( "settingsIcon" );
	timer  = popup->findChild<QObject *>( "closeTimer" );
	icon->setProperty( "source", "qrc:///icons/battery-000.svg" );

	qApp->processEvents();

	qDebug() << "UPower watcher started";
};

void PowerManager::onPowerStateChanged( Solid::Battery::ChargeState state ) {

	switch( state ) {
		case Solid::Battery::NoCharge: {
			icon->setProperty( "source", "qrc:///icons/battery-000.svg" );
			qDebug() << "Battery out of charge";
			break;
		}
		case Solid::Battery::Charging: {
			icon->setProperty( "source", "qrc:///icons/power-on.svg" );
			qDebug() << "Battery is charging";
			break;
		}
		case Solid::Battery::Discharging: {
			icon->setProperty( "source", "qrc:///icons/power-off.svg" );
			qDebug() << "Battery is discharging";
			// onChargeChanged();
			break;
		}
		case Solid::Battery::FullyCharged: {
			icon->setProperty( "source", "qrc:///icons/power-on.svg" );
			qDebug() << "Power supply connected";
			break;
		}
	}

	/* Popup is not visible: Show it and start the close timer */
	if ( not popup->property( "visible" ).toBool() ) {
		QMetaObject::invokeMethod( popup, "show"  );
		QMetaObject::invokeMethod( timer, "start"  );
		qApp->processEvents();
	}

	/* Already visible, extend the timer */
	else {
		QMetaObject::invokeMethod( timer, "restart"  );
	}
};

void PowerManager::onChargeChanged() {

	Solid::Battery* battery = qobject_cast<Solid::Battery *>( sender() );

	if ( not battery )
		return;

	/* Check for the change in the charge state */
	int batIdx = mBatteries.indexOf( battery );
	int chargeState = mBatteryStates.value( batIdx );

	if ( chargeState != battery->chargeState() ) {
		mBatteryStates[ batIdx ] = battery->chargeState();
		onPowerStateChanged( battery->chargeState() );
		return;
	}

	int charge = battery->chargePercent();
	switch( charge ) {
		case 0: {
			icon->setProperty( "source", "qrc:///icons/battery-000.svg" );
			break;
		}
		case 5: {
			icon->setProperty( "source", "qrc:///icons/battery-005.svg" );
			break;
		}
		case 10: {
			icon->setProperty( "source", "qrc:///icons/battery-010.svg" );
			break;
		}
		case 20: {
			icon->setProperty( "source", "qrc:///icons/battery-020.svg" );
			break;
		}
		case 40: {
			icon->setProperty( "source", "qrc:///icons/battery-040.svg" );
			break;
		}
		case 60: {
			icon->setProperty( "source", "qrc:///icons/battery-060.svg" );
			break;
		}
		case 80: {
			icon->setProperty( "source", "qrc:///icons/battery-080.svg" );
			break;
		}
		case 100: {
			icon->setProperty( "source", "qrc:///icons/battery-100.svg" );
			break;
		}
		default: {
			qDebug() << "Battery at" << charge << "\b% and" << ( battery->chargeState() == Solid::Battery::Discharging ? "discharging" : "charging" );
			return;
		}
	}

	/* Popup is not visible: Show it and start the close timer */
	if ( not popup->property( "visible" ).toBool() ) {
		QMetaObject::invokeMethod( popup, "show"  );
		QMetaObject::invokeMethod( timer, "start"  );
		qApp->processEvents();
	}

	/* Already visible, extend the timer */
	else {
		QMetaObject::invokeMethod( timer, "restart"  );
	}

	popup->setProperty( "visible", true );
};
