/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	* Qt5 example of analog clock, and Lenovo K8 Note's clock widget
	* are the inspiration for this. Some parts of the code are taken
	* from Qt5's example
	*
	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*
	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*
	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QQmlComponent>
#include <QtWidgets>

#include "AudioManager.hpp"

PactlQtDevice::PactlQtDevice( QString name, int id ) {

	mDeviceId = id;
	mName = name;
};

QString PactlQtDevice::name() {

	return mName;
};

int PactlQtDevice::currentVolume() {

	QProcess proc;
	proc.start( "pactl", QStringList() << "list" << "sinks" );
	proc.waitForFinished();

	QStringList data = QString::fromLocal8Bit( proc.readAll() ).split( "\n", QString::SkipEmptyParts );

	Q_FOREACH( QString chunk, data ) {
		if ( chunk.simplified().trimmed().startsWith( "Volume" ) ) {
			QStringList chunks = chunk.simplified().trimmed().split( " ", QString::SkipEmptyParts );
			int left = chunks.value( 4, "-1" ).replace( "%", "" ).toInt();
			int right = chunks.value( 11, "-1" ).replace( "%", "" ).toInt();

			if ( ( left >= 0 ) and ( right >= 0 ) )
                return static_cast<int>( left + right ) / 2;
		}
	}

	return 0;
};

bool PactlQtDevice::isMuted() {

	QProcess proc;
	proc.start( "pactl", QStringList() << "list" << "sinks" );
	proc.waitForFinished();

	QStringList data = QString::fromLocal8Bit( proc.readAll() ).split( "\n", QString::SkipEmptyParts );
	Q_FOREACH( QString chunk, data ) {
		if ( chunk.simplified().trimmed().startsWith( "Mute:" ) ) {
			QStringList chunks = chunk.simplified().trimmed().split( " ", QString::SkipEmptyParts );

			if ( chunks.value( 1 ) == "yes" )
				return true;

			else
				return false;
		}
	}

	/* By default, treat it as muted */
	return true;
};

void PactlQtDevice::setVolume( int volume ) {

	QProcess proc;
	proc.start( "pactl", { "set-sink-volume", "@DEFAULT_SINK@", QString( "%1%" ).arg( volume ) } );
	proc.waitForFinished( 1000 );

	currentVolume();
};

void PactlQtDevice::toggleMute() {

	QProcess proc;
	proc.startDetached( "pactl", QStringList() << "set-sink-mute" << "@DEFAULT_SINK@" << QString( "toggle" ) );
};

AudioManager *AudioManager::m_mgr = nullptr;

AudioManager* AudioManager::instance() {

	if ( m_mgr == nullptr )
		m_mgr = new AudioManager();

	return m_mgr;
};

AudioManager::AudioManager() : QObject() {

	QProcess proc;
	proc.start( "pactl", QStringList() << "list" << "sinks" );
	proc.waitForFinished();

	QStringList devProps = QString::fromLocal8Bit( proc.readAll() ).split( "\n\n", QString::SkipEmptyParts );

	// To get the name of the device: First device is the default device
	QStringList props = devProps.value( 0 ).split( "\n", QString::SkipEmptyParts );
	QString name;
	Q_FOREACH( QString prop, props ) {
		if ( prop.trimmed().simplified().startsWith( "device.description" ) ) {
			name = prop.trimmed().simplified().replace( "\"", "" ).split( " = ", QString::SkipEmptyParts ).value( 1 );
			break;
		}
	}

	device = new PactlQtDevice( name, 0 );

	const QUrl url( "qrc:/SettingsPopup.qml" );
	view = new QQmlApplicationEngine();
	qApp->processEvents();

	QQmlComponent component( view, url );
	popup = component.create();
	icon  = popup->findChild<QObject *>( "settingsIcon" );
	slider = popup->findChild<QObject *>( "slider" );
	timer = popup->findChild<QObject *>( "closeTimer" );

	slider->setProperty( "value", device->currentVolume() / 100.0 );
	qApp->processEvents();
};

void AudioManager::toggleMute() {

	int curVol = device->currentVolume();

	/* Already muted, unmute */
	if ( device->isMuted() ) {

		qDebug() << "unmute" << curVol;

		/* Enable the slider */
		slider->setProperty( "enabled", true );

		/* Proper icon: low, medium or high */
		if ( ( 0 <= curVol ) and ( curVol < 40 ) )
			icon->setProperty( "source", "qrc:///icons/audio-volume-low.svg" );

		else if ( ( 40 <= curVol ) and ( curVol < 80 ) )
			icon->setProperty( "source", "qrc:///icons/audio-volume-medium.svg" );

		else
			icon->setProperty( "source", "qrc:///icons/audio-volume-high.svg" );

		slider->setProperty( "value", curVol / 100.0 );
	}

	else {
		qDebug() << "mute";

		slider->setProperty( "enabled", false );
		icon->setProperty( "source", "qrc:///icons/audio-volume-muted.svg" );
	}

	/* Popup is not visible: Show it and start the close timer */
	if ( not popup->property( "visible" ).toBool() ) {
		QMetaObject::invokeMethod( popup, "show"  );
		QMetaObject::invokeMethod( timer, "start"  );
		qApp->processEvents();
	}

	/* Already visible, extend the timer */
	else {
		QMetaObject::invokeMethod( timer, "restart"  );
	}

	popup->setProperty( "visible", true );
	device->toggleMute();
};

void AudioManager::increaseVolume() {

	int curVol = device->currentVolume();

	int newVol = curVol + 10;
	if ( newVol > 100 )
		newVol = 100;

	if ( ( 0 <= newVol ) and ( newVol < 40 ) )
		icon->setProperty( "source", "qrc:///icons/audio-volume-low.svg" );

	else if ( ( 40 <= newVol ) and ( newVol < 80 ) )
		icon->setProperty( "source", "qrc:///icons/audio-volume-medium.svg" );

	else
		icon->setProperty( "source", "qrc:///icons/audio-volume-high.svg" );

	/* Popup is not visible: Show it and start the close timer */
	if ( not popup->property( "visible" ).toBool() ) {
		QMetaObject::invokeMethod( popup, "show"  );
		QMetaObject::invokeMethod( timer, "start"  );
		qApp->processEvents();
	}

	/* Already visible, extend the timer */
	else {
		QMetaObject::invokeMethod( timer, "restart"  );
	}

	slider->setProperty( "value", newVol / 100.0 );
	device->setVolume( newVol );

	qDebug() << "Volume: " << newVol << device->currentVolume();
};

void AudioManager::decreaseVolume() {

	int curVol = device->currentVolume();

	int newVol = curVol - 10;
	if ( newVol < 0 )
		newVol = 0;

	if ( ( 0 <= newVol ) and ( newVol < 40 ) )
		icon->setProperty( "source", "qrc:///icons/audio-volume-low.svg" );

	else if ( ( 40 <= newVol ) and ( newVol < 80 ) )
		icon->setProperty( "source", "qrc:///icons/audio-volume-medium.svg" );

	else
		icon->setProperty( "source", "qrc:///icons/audio-volume-high.svg" );

	/* Popup is not visible: Show it and start the close timer */
	if ( not popup->property( "visible" ).toBool() ) {
		QMetaObject::invokeMethod( popup, "show"  );
		QMetaObject::invokeMethod( timer, "start"  );
		qApp->processEvents();
	}

	/* Already visible, extend the timer */
	else {
		QMetaObject::invokeMethod( timer, "restart"  );
	}

	slider->setProperty( "value", newVol / 100.0 );
	qApp->processEvents();

	device->setVolume( newVol );

	qDebug() << "Volume: " << newVol << device->currentVolume();
};
