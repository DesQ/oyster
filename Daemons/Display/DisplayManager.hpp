/*
	*
	* Copyright 2020 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QWidget>
#include <QLabel>
#include <QProgressBar>

#include <QQmlApplicationEngine>

class BacklightDevice {

	public:
		BacklightDevice( QString path );

		qreal currentBrightness() const;
		bool setBrightness( qreal value ) const;

		bool increaseBrightness() const;
		bool decreaseBrightness() const;

	private:
		QString devPath;
		qreal maxBrightness;
		qreal step;
};

typedef QList<BacklightDevice> BacklightDevices;

class DisplayManager : public QObject {
	Q_OBJECT

	public:
		static DisplayManager *instance();

		void increaseBrightness();
		void decreaseBrightness();

	private:
		DisplayManager();

		static DisplayManager *m_mgr;
		QQmlApplicationEngine *view;

		QObject *popup;
		QObject *icon;
		QObject *slider;
		QObject *timer;

		BacklightDevices backlights;
};
