/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	* Qt5 example of analog clock, and Lenovo K8 Note's clock widget
	* are the inspiration for this. Some parts of the code are taken
	* from Qt5's example
	*
	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*
	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*
	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QQmlComponent>
#include <QtWidgets>

#include "DisplayManager.hpp"

BacklightDevice::BacklightDevice( QString path ) {

    devPath = QString( path );

    QFile curFile( devPath + "/max_brightness" );

	maxBrightness = 255.0;

    if ( curFile.open( QIODevice::ReadOnly ) ) {
        maxBrightness = QString::fromLocal8Bit( curFile.readAll() ).simplified().toDouble();
		step = 0.05 * maxBrightness;
	}

	curFile.close();
};

qreal BacklightDevice::currentBrightness() const {

	/* Brightness scaled to 1 */
    QFile curFile( devPath + "/brightness" );

    if ( not curFile.open( QIODevice::ReadOnly ) ) {
		return -1;
    }

    qreal curBrightness = QString::fromLocal8Bit( curFile.readAll() ).simplified().toDouble();
	curFile.close();

    return curBrightness / maxBrightness;
};

bool BacklightDevice::setBrightness( qreal value ) const {
	/* @value is in range 0 - 1 */

    QFile curFile(devPath + "/brightness");

    if ( not curFile.open( QIODevice::WriteOnly ) )
		return false;

	/* If the change in brightness is less than 1% */
	qreal curBrightness = currentBrightness() * maxBrightness;
    qreal brightness = value * maxBrightness;

	brightness = brightness < step ? step : ( brightness > maxBrightness ? maxBrightness : brightness );

	if ( abs( curBrightness - brightness ) < 1 )
		return true;

    curFile.write( QByteArray::number( ( int )brightness ) );
	curFile.close();

	qDebug() << brightness << maxBrightness;

	return true;
};

bool BacklightDevice::increaseBrightness() const {

	qreal curBright = currentBrightness() * maxBrightness;
	curBright += step;

	return setBrightness( curBright / maxBrightness );
};

bool BacklightDevice::decreaseBrightness() const {

	qreal curBright = currentBrightness() * maxBrightness;
	curBright -= step;

	return setBrightness( curBright / maxBrightness );
};

DisplayManager *DisplayManager::m_mgr = nullptr;

DisplayManager* DisplayManager::instance() {

	if ( m_mgr == nullptr )
		m_mgr = new DisplayManager();

	return m_mgr;
};

DisplayManager::DisplayManager() : QObject() {

	QDir blDir( "/sys/class/backlight/" );
	Q_FOREACH( QString bl, blDir.entryList( QDir::NoDotAndDotDot | QDir::AllEntries ) ) {
        backlights << BacklightDevice( blDir.filePath( bl ) );
    }

	for( int i = 1; i < backlights.count(); i++ )
		backlights.at( i ).setBrightness( backlights.at( 0 ).currentBrightness() );

	const QUrl url( "qrc:/SettingsPopup.qml" );
	view = new QQmlApplicationEngine();
	qApp->processEvents();

	QQmlComponent component( view, url );
	popup = component.create();

	icon  = popup->findChild<QObject *>( "settingsIcon" );
	icon->setProperty( "source", "qrc:///icons/monitor.svg" );

	slider = popup->findChild<QObject *>( "slider" );
	slider->setProperty( "value", backlights.at( 0  ).currentBrightness() );

	timer = popup->findChild<QObject *>( "closeTimer" );

	qApp->processEvents();
};

void DisplayManager::increaseBrightness() {

	/* Popup is not visible: Show it and start the close timer */
	if ( not popup->property( "visible" ).toBool() ) {
		QMetaObject::invokeMethod( popup, "show"  );
		QMetaObject::invokeMethod( timer, "start"  );
		qApp->processEvents();
	}

	/* Already visible, extend the timer */
	else {
		QMetaObject::invokeMethod( timer, "restart"  );
		qApp->processEvents();
	}

	backlights.at( 0 ).increaseBrightness();
	for( int i = 1; i < backlights.count(); i++ )
		backlights.at( i ).setBrightness( backlights.at( 0 ).currentBrightness() );

	slider->setProperty( "value", backlights.at( 0 ).currentBrightness() );
};

void DisplayManager::decreaseBrightness() {

	/* Popup is not visible: Show it and start the close timer */
	if ( not popup->property( "visible" ).toBool() ) {
		QMetaObject::invokeMethod( popup, "show"  );
		QMetaObject::invokeMethod( timer, "start"  );
		qApp->processEvents();
	}

	/* Already visible, extend the timer */
	else {
		QMetaObject::invokeMethod( timer, "restart"  );
		qApp->processEvents();
	}

	backlights.at( 0 ).decreaseBrightness();
	for( int i = 1; i < backlights.count(); i++ )
		backlights.at( i ).setBrightness( backlights.at( 0 ).currentBrightness() );

	slider->setProperty( "value", backlights.at( 0 ).currentBrightness() );
};
