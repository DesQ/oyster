TEMPLATE = app
TARGET = oyster-shell

QT += widgets dbus svg quick

INCLUDEPATH += /usr/include/libdesq/ /usr/include/libdesqui/
LIBS += -ldesq -ldesqui -ldesq-x11 -lX11

INCLUDEPATH += . Shell X11
DEPENDPATH += . Shell X11 icons

# Headers
HEADERS += Shell/OysterShell.hpp
HEADERS += X11/HotKeyManager.hpp
HEADERS += X11/PresentWindows.hpp
HEADERS += X11/X11Handler.hpp

# Sources
SOURCES += Shell/OysterShell.cpp
SOURCES += X11/HotKeyManager.cpp
SOURCES += X11/PresentWindows.cpp
SOURCES += X11/X11Handler.cpp
SOURCES += X11/xhklib.c
SOURCES += Main.cpp

#
# Widgets
#

# Clock Widget
INCLUDEPATH += Widgets/Clock
DEPENDPATH += Widgets/Clock
HEADERS += Widgets/Clock/ClockWidget.hpp
SOURCES += Widgets/Clock/ClockWidget.cpp

# Menu Widget
INCLUDEPATH += Widgets/Menu
DEPENDPATH += Widgets/Menu
HEADERS += Widgets/Menu/OysterMenu.hpp
HEADERS += Widgets/Menu/OysterMenuViews.hpp
SOURCES += Widgets/Menu/OysterMenu.cpp
SOURCES += Widgets/Menu/OysterMenuViews.cpp

# Settings Widgets
INCLUDEPATH += Widgets/Popups
DEPENDPATH += Widgets/Popups
RESOURCES += Widgets/Popups/SettingsPopup.qrc

#
# Daemons
#

# Audio
INCLUDEPATH += Daemons/Audio
DEPENDPATH += Daemons/Audio
HEADERS += Daemons/Audio/AudioManager.hpp
SOURCES += Daemons/Audio/AudioManager.cpp

# Display
INCLUDEPATH += Daemons/Display
DEPENDPATH += Daemons/Display
HEADERS += Daemons/Display/DisplayManager.hpp
SOURCES += Daemons/Display/DisplayManager.cpp

# Power
INCLUDEPATH += Daemons/Power /usr/include/KF5/Solid/
LIBS += -lKF5Solid

DEPENDPATH += Daemons/Power
HEADERS += Daemons/Power/PowerManager.hpp
SOURCES += Daemons/Power/PowerManager.cpp

# Resources
RESOURCES += icons/icons.qrc

# Silent compilation
CONFIG += silent

# Build location
BUILD_PREFIX = $$(Oyster_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
	BUILD_PREFIX = ./build
}

MOC_DIR 	= $$BUILD_PREFIX/Oyster/Shell/moc
OBJECTS_DIR = $$BUILD_PREFIX/Oyster/Shell/obj
RCC_DIR		= $$BUILD_PREFIX/Oyster/Shell/qrc
UI_DIR      = $$BUILD_PREFIX/Oyster/Shell/uic

unix {
	isEmpty(PREFIX) {
		PREFIX = /usr
	}
	BINDIR = $$PREFIX/bin/

	INSTALLS += target icons bg data
	target.path = $$BINDIR

	icons.path = $$PREFIX/share/icons/hicolor/256x256/apps/
	icons.files = icons/oyster.png

	bg.path = $$PREFIX/share/oyster/resources/
	bg.files = resources/oyster.jpg

	data.path = $$PREFIX/share/oyster-shell/
	data.files = README.md Changelog ReleaseNotes
}
