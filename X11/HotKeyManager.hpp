/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QThread>
extern "C" {
	#include "xhklib.h"
}

class HotKeyManager : public QThread {
	Q_OBJECT

	public:
		static HotKeyManager* instance();

		void registerHotKey( unsigned int modifiers, KeySym keysym );
		void unregisterHotKey( unsigned int modifiers, KeySym keysym );

		static void emitSignal( xhkEvent e, void *, void *, void * );

		void stop();

	protected:
		void run();

	private:
		HotKeyManager();

		bool mAllowEmit;
		int mCurrentCorner;

		xhkConfig *hkconfig;

		static HotKeyManager *hkMgr;

	Q_SIGNALS:
		void hotKeyPressed( quint32 id );
};
