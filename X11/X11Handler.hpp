/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QListWidget>
#include <libdesq/DesQClient.hpp>
#include <libdesq/DesQWMSession.hpp>

class PresentWindows;

class X11Handler : public QObject {
    Q_OBJECT

    public:
		/* Current X11 session */
        static X11Handler* session();

        /* Shutting the plugin */
        ~X11Handler();

        /* Show the client list */
        void showClientList();

        /* Show desktop */
        void showDesktop();

        /* Show/unshow the desktop */
        QSize desktopSize();

    private:
        PresentWindows *mAppSwitcher = nullptr;
        DesQWMSession *mSession = nullptr;
        Windows oldList;

        static bool mInit;
        bool touch = false;
        bool showDesktopBool = false;

        QHash<Window, bool> wLists;
        Window activeWinID;

		X11Handler();
		static X11Handler* mX11Handler;

    public Q_SLOTS:
		/* Signal for OysterShell to activate a given window */
        void activateWindow( QListWidgetItem * );

		/* Signal for OysterShell to close the active app */
		void closeActiveApp( QListWidgetItem *item );

	Q_SIGNALS:
		/* Signal for OysterShell to resize to occupy full available space */
        void resizeDesktop();

		/* Signal for mouse entering hot corner */
		void hotCornerActivated( int );

		/* Signal for mouse leaving hot corner */
		void hotCornerDeactivated( int );
};
