/*
	*
	* This file is a part of CoreStuff.
	* An activity viewer for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "X11Handler.hpp"
#include "PresentWindows.hpp"

#include <QWidget>
#include <QToolButton>
#include <QHBoxLayout>
#include <QLabel>

#include <libdesq/DesQClient.hpp>
#include <libdesq/DesQWMSession.hpp>
#include <libdesq/DesQEventFilter.hpp>

X11Handler* X11Handler::mX11Handler = nullptr;
bool X11Handler::mInit = false;

X11Handler* X11Handler::session() {

	if ( not mInit )
		mX11Handler = new X11Handler();

	return mX11Handler;
}

X11Handler::X11Handler() {

	mSession = DesQWMSession::currentSession();
	connect( mSession, &DesQWMSession::resizeDesktop, this, &X11Handler::resizeDesktop );

	mAppSwitcher = new PresentWindows();
	connect( mAppSwitcher, &PresentWindows::itemClicked, this, &X11Handler::activateWindow );

	DesQMouseCatcher *mc = new DesQMouseCatcher();
	connect( mc, &DesQMouseCatcher::hotCornerActivated, this, &X11Handler::hotCornerActivated );
	connect( mc, &DesQMouseCatcher::hotCornerDeactivated, this, &X11Handler::hotCornerDeactivated );
	mc->start();

	mInit = true;
};

X11Handler::~X11Handler() {

};

void X11Handler::activateWindow( QListWidgetItem *item ) {

	if ( item ) {
		Window winID = ( Window )item->data( Qt::UserRole + 1 ).toLongLong();

		if ( winID > 0 ) {
			DesQClient client( winID );
			client.activate();
		}
	}

	mAppSwitcher->close();
};

void X11Handler::closeActiveApp( QListWidgetItem *item ) {

	if ( item ) {
		Window winID = static_cast<unsigned long>( item->data( Qt::UserRole + 1 ).toLongLong() );
		if ( winID > 0 ) {
			DesQClient client( winID );
			client.close();

			delete item;
		}
	}
};

void X11Handler::showClientList() {

	Windows clients = DesQWMSession::currentSession()->listClients();

	if ( not clients.count() )
		return;

	mAppSwitcher->clear();

	Q_FOREACH( Window win, clients ) {
		DesQClient client( win );

		if ( client.type() != DesQClient::Normal )
			continue;

		QListWidgetItem *itm = new QListWidgetItem( QIcon( QPixmap::fromImage( client.screengrab() ) ), client.title(), mAppSwitcher );
		itm->setData( Qt::UserRole + 1, (unsigned long long)win );
		mAppSwitcher->addItem( itm );
	}

	mAppSwitcher->show();
};

void X11Handler::showDesktop() {

	mSession->showDesktop();
};

QSize X11Handler::desktopSize() {

	return mSession->desktopSize();
};
