/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QImage>
#include <QListWidget>

class PresentWindows : public QListWidget {
	Q_OBJECT

	public:
		PresentWindows();

		void show();

	private:
		QListWidget *appsWidget;
		QPixmap bgImage;

	protected:
		/* Overrides to quit this window */
		void mouseReleaseEvent( QMouseEvent *mEvent );
		void keyPressEvent( QKeyEvent *kEvent );

		/* Reset the grid size on resize */
		void resizeEvent( QResizeEvent *rEvent );

		/* Draw the background */
		void paintEvent( QPaintEvent *pEvent );

		/* To emit closed() signal */
		void closeEvent( QCloseEvent *cEvent );

	Q_SIGNALS:
		void closed();
};
