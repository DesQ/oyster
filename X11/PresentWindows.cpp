/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "PresentWindows.hpp"

#include <libdesq/DesQSettings.hpp>
#include <libdesq/DesQClient.hpp>
#include <libdesq/DesQWMSession.hpp>

PresentWindows::PresentWindows() : QListWidget() {

	setFixedSize( qApp->primaryScreen()->size() );
	setWindowFlags(
		Qt::FramelessWindowHint |
		Qt::WindowStaysOnTopHint |
		Qt::X11BypassWindowManagerHint |
		Qt::NoDropShadowWindowHint
	);

	setAttribute( Qt::WA_X11NetWmWindowTypeDock );

	setViewMode( QListView::IconMode );
	setIconSize( QSize( 512, 512 ) );
	setGridSize( QSize( viewport()->width() - 10, 312 ) );
	setFrameStyle( QFrame::NoFrame );
	viewport()->setAutoFillBackground( false );
	setUniformItemSizes( true );
	setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
	setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

	QString imageLoc = ( QString )DesQSettings::instance( "Oyster", "Shell" )->value( "Background" );
	bgImage = QPixmap( imageLoc ).scaled( qApp->primaryScreen()->size(),  Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation );

	setWindowTitle( "Oyster Shell | Present Windows" );

	connect( this, &QListWidget::itemClicked, [=]( QListWidgetItem *item ) {
			Window wid = item->data( Qt::UserRole + 1 ).toULongLong();
			DesQClient client( wid );
			client.activate();

			close();
		}
	);
};

void PresentWindows::show() {

	QListWidget::show();

	qApp->processEvents();

	setFocus();
	viewport()->grabMouse();
	grabKeyboard();
};

void PresentWindows::mouseReleaseEvent( QMouseEvent *mEvent ) {

	if ( not itemAt( mEvent->pos() ) )
		close();

	QListWidget::mouseReleaseEvent( mEvent );
};

void PresentWindows::keyPressEvent( QKeyEvent *kEvent ) {

	if ( kEvent->key() == Qt::Key_Escape )
		close();

	QListWidget::keyPressEvent( kEvent );
};

void PresentWindows::resizeEvent( QResizeEvent *rEvent ) {

	QListWidget::resizeEvent( rEvent );

	/* New grid size */
	setGridSize( QSize( viewport()->width() - 10, 312 ) );
};

void PresentWindows::paintEvent( QPaintEvent *pEvent ) {

	QPainter painter( viewport() );
	painter.drawPixmap( geometry(), bgImage, QRect( QPoint( ( bgImage.width() - width() ) / 2, ( bgImage.height() - height() ) / 2 ), size() ) );
	painter.end();

	QListWidget::paintEvent( pEvent );

	pEvent->accept();
};

void PresentWindows::closeEvent( QCloseEvent *cEvent ) {

	emit closed();

	viewport()->releaseMouse();
	releaseKeyboard();

	cEvent->accept();
};
