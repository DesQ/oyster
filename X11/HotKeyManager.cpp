/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "Global.hpp"
#include "HotKeyManager.hpp"

extern "C" {
	#include "xhklib.h"
}

#include <cstdlib>

HotKeyManager* HotKeyManager::hkMgr = NULL;

HotKeyManager *HotKeyManager::instance() {

	if ( not hkMgr )
		hkMgr = new HotKeyManager();

	return hkMgr;
};

HotKeyManager::HotKeyManager() {

	hkconfig = xhkInit( NULL );
};

void HotKeyManager::stop() {

	xhkClose( hkconfig );
};

void HotKeyManager::registerHotKey( unsigned int modifiers, KeySym keysym ) {

	xhkBindKey( hkconfig, 0, keysym, modifiers, xhkKeyRelease, HotKeyManager::emitSignal, 0, 0, 0 );
};

void HotKeyManager::unregisterHotKey( unsigned int modifiers, KeySym keysym ) {

	xhkUnBindKey( hkconfig, 0, keysym, modifiers, xhkKeyRelease );
};

void HotKeyManager::emitSignal( xhkEvent e, void *, void *, void * ) {

	emit hkMgr->hotKeyPressed( e.modifiers | e.keysym );
};

void HotKeyManager::run() {

	while ( true ) {
        xhkPollKeys( hkconfig, 1 );
    }
};
